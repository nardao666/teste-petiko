<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        $string1 = $word;
		$string2 = '';
		
		for ($i = strlen($string1); $i >= 0; $i--) {
			$ii = strlen($string1)-$i;
			$string2[$ii] = $string1[$i];
		}

		if (strcomp($string1,$string2) == 0){
			return TRUE;
		}else{
			return FALSE;
		}
    }
}

echo Palindrome::isPalindrome('Deleveled');